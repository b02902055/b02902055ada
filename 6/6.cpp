#include <stdio.h>
#include <algorithm>
#define FAIL 0
#define SUCC 1
using namespace std;

int cityNum, airportNum, city[100010], airport[100010];
bool compare(int a, int b){
	return a < b;
}
void print_airport(){
	puts("these city has airport: ");
	for (int i = 0; i < cityNum; i++)
		if (airport[i])
			printf("%d ", i);
	puts("");
	return;
}
void print_city(){
	for (int i = 0; i < cityNum; i++)
		printf("%d ", city[i]);
	puts("");
	return;
}
bool check(int mid){
	//if my airport < airport return fail
	int myairportNum = 0;
	int lastCity = 0;
	int hasAirport = 0; //no airport
	for (int i = 0; i < cityNum; i++)
		airport[i] = 0;
	for (int i = 1; i < cityNum; i++){
		if (city[i] - city[lastCity] < mid){
			if (i == cityNum - 1 && !hasAirport){
				myairportNum++;
				airport[i] = 1;	
				break;
			}	
			continue;
		}
		else if (city[i] - city[lastCity] == mid){
			if (!hasAirport){
				if (i == cityNum - 1){
					myairportNum++;
					airport[i] = 1;
					break;				
				}
				myairportNum++;
				airport[i] = 1;
				hasAirport = 1;
				lastCity = i;
			}
			else {
				hasAirport = 0;
				lastCity = i + 1;
			}
		}
		else if (city[i] - city[lastCity] > mid){
			if (!hasAirport){
				myairportNum += 1;
				airport[i - 1] = 1;
				hasAirport = 1;
				lastCity = i - 1;
				i--;
			}
			else {
				if (i == cityNum - 1){
					myairportNum++;
					airport[i] = 1;
					break;
				}
				lastCity = i;
				hasAirport = 0;				
			}
		}		
	}
//	printf("mid = %d\n", mid);
//	print_airport();
//	printf("myairportNum = %d\n", myairportNum);
//	printf("airportNum = %d\n", airportNum);
	if (myairportNum <= airportNum)
		return FAIL;
	return SUCC;
}
void find_min_max(){
	int l = 0, r = city[cityNum - 1] - city[0] + 1;
	int mid;
	int status;
	while (l < r){
		mid = (l + r) / 2;
		status = check(mid);
		if (status == FAIL)	//mid is too large now
			r = mid + 1;
		else if(status == SUCC)
			l = mid + 1;	
	}
	status = check(mid);
	if (status == FAIL)
		printf("%d\n", mid);
	else if (status == SUCC)
		printf("%d\n", mid + 1);
	return;
}
int main(void){
	int testCase;
	scanf("%d", &testCase);
	while (testCase--){
		scanf("%d%d", &cityNum, &airportNum);
		for (int i = 0; i < cityNum; i++)
			scanf("%d", &city[i]);
		sort(city, &city[cityNum], compare);
//		print_city();	
//		for (int i = 0; i <= city[cityNum - 1] - city[0]; i++){
//			check(i);
//			puts("");
//		}
		find_min_max();
	}
	return 0;
}
