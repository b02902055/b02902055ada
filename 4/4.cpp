#include <stdio.h>
#include <string.h>
char x[2005], y[2005];
short c[2005][2005];
short tx[2005][26], ty[2005][26];
int lx, ly;

void print_t(char a){
	if (a == 'c')
		for (int i = 0; i <= ly; i++){
			for (int j = 0; j <= lx; j++)
				printf("%d ", c[j][i]);
			puts("");
		}
	else if (a == 'x')
		for (int i = 0; i < lx; i++){
			for (int j = 0; j < 26; j++)
				printf("%d ", tx[i][j]);
			puts("");
		}
	else if (a == 'y')
		for (int i = 0; i < ly; i++){
			for (int j = 0; j < 26; j++)
				printf("%d ", ty[i][j]);
			puts("");
		}
	return;
}
void rev(){
	char t;
	for (int i = 0; i < lx / 2; i++){
		t = x[i];
		x[i] = x[lx - i - 1];
		x[lx - i - 1] = t;
	}
	for (int i = 0; i < ly / 2; i++){
		t = y[i];
		y[i] = y[ly - i - 1];
		y[ly - i - 1] = t;
	}
	return;
}
void mkTable(){
	rev();
	// init
	for (int i = 0; i <= lx; i++)
		c[i][0] = 0;
	for (int i = 0; i <= ly; i++)
		c[0][i] = 0;
	for (int i = 0; i < lx; i++)
		for (int j = 0; j < 26; j++)
			tx[i][j] = -1;
	for (int i = 0; i < ly; i++)
		for (int j = 0; j < 26; j++)
			ty[i][j] = -1;
	// make c
	for (int i = 1; i <= ly; i++)
		for (int j = 1; j <= lx; j++){
			if (x[j - 1] == y[i - 1])
				c[j][i] = c[j - 1][i - 1] + 1;
			else 
				c[j][i] = (c[j][i - 1] > c[j - 1][i]) ? c[j][i - 1] : c[j - 1][i];
		}
	//make tx, ty
	tx[0][x[0] - 'a'] = 0;
	for (int i = 1; i < lx; i++){
		for (int j = 0; j < 26; j++)
			tx[i][j] = tx[i - 1][j];
		tx[i][x[i] - 'a'] = i;
	}
	ty[0][y[0] - 'a'] = 0;
	for (int i = 1; i < ly; i++){
		for (int j = 0; j < 26; j++)
			ty[i][j] = ty[i - 1][j];
		ty[i][y[i] - 'a'] = i;
	}
//	print_t('x');
//	puts("");
//	print_t('y');
	lx--;
	ly--;
	return;
}
void p_ans(int len){
//	printf("\nlen = %d lx = %d ly = %d", len, lx, ly);
	if (len == 0){
		puts("");
		return;
	}
	for (int i = 0; i < 26; i++){
		int nx = tx[lx][i];
		int ny = ty[ly][i];
		if (c[nx + 1][ny + 1] == len){
			printf("%c", 'a' + i);
			lx = nx - 1;
			ly = ny - 1;
			p_ans(len - 1);
			return;
		}
	}
	return;
}
int main(void){
	int t;
	scanf("%d", &t);
	while (t--){
		scanf("%s%s", x, y);
		lx = strlen(x);
		ly = strlen(y);
		mkTable();
		p_ans(c[lx + 1][ly + 1]);
	}
	return 0;
}
