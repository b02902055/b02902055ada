#include <stdio.h>
#include <string.h>
#define mod 1000000007
long long int dp[17][17][33000];
void printb(int num){
	if (num == 0){
		puts("0");
		return;
	}
	int digit = 0;
	int a[20];
	for (; num != 0; num /= 2){
		a[digit] = num % 2;
		digit++;
	}
	for (digit -= 1; digit >= 0; digit--)
		printf("%d", a[digit]);
	puts("");
	return;
}
int power(int n){
	int ans = 1;
	for (int i = 0; i < n; i++)
		ans *= 2;
	return ans;
}
int status_digit(int status, int digit){
	return ((status&(1<<(digit - 1)))>>(digit - 1));
}

int main(void){
	int testCase;
	char map[17][17];
	int n, m, status;
	int t1, t2, k, a, b, c, d, upperbound;
	scanf("%d", &testCase);
	while (testCase--){
		// init
		memset(map, 'X', sizeof(map));
		scanf("%d%d", &n, &m);
		for (int i = 0; i < n; i++)
			scanf("%s", map[i]);
		memset(dp, 0, sizeof(dp));
		dp[0][m - 1][0] = 1;
		upperbound = power(m);
		// solve
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++){
				k = m - j - 1;
				t1 = (1<<j);
				t2 = (3<<j);
				for (status = 0; status < upperbound; status++){

					// info
//					puts("===========================");
//					printf("i = %d, k = %d satus = ", i, k);
//					printb(status);
					// mod
					if (i % 2 == 1)
						dp[i][k][status] %= mod;
					a = (status|t1);
					b = (status|t2);
					c = (status|t1) - t1;
					d = (status|t2) - t2;

					// 1 * 1
//					puts("filling 1 * 1");
//					printf("1 * 1: ");
//					printb((status|t1) - t1);
					if (j + 1 == m)
						dp[i + 1][m - 1][c] += dp[i][k][status];
					else
						dp[i][k - 1][c] += dp[i][k][status];

					
					if (map[i][k] == 'X' || status_digit(status, j + 1))
						continue;	
					// 1 * 2
					if (j <= m - 2 && map[i][k - 1] == '.' && status_digit(status, j + 2) == 0){
//						puts("filling 1 * 2");
//						printf("1 * 2: ");
//						printb((status|t2) - t2);
						if (j == m - 2)
							dp[i + 1][m - 1][d] += dp[i][k][status];
						else
							dp[i][k - 2][d] += dp[i][k][status];				
					}
					// 2 * 1
					if (map[i + 1][k] == '.'){
//						puts("filling 2 * 1");
//						printf("2 * 1: ");
//						printb(status|t1);
						if (j + 1 == m)
							dp[i + 1][m - 1][a] += dp[i][k][status];
						else
							dp[i][k - 1][a] += dp[i][k][status];
					}

					// 2 * 2
					if (j <= m - 2 && map[i + 1][k] == '.' && map[i][k - 1] == '.' &&
							map[i + 1][k - 1] == '.' && status_digit(status, j + 2) == 0){
//						puts("filling 2 * 1");
//						printf("2 * 2: ");
//						printb(status|t2);
						if (j == m - 2)
							dp[i + 1][m - 1][b] += dp[i][k][status];
						else
							dp[i][k - 2][b] += dp[i][k][status];
					}
//					printf("%d %d %d %d %d\n", dp[1][0][0], dp[1][0][1], dp[2][0][0], dp[2][0][1], dp[3][0][0]);
//					printf("ans = %lld\n", dp[n][m - 1][0]);
				}		
			}
		printf("%lld\n", dp[n][m - 1][0] % mod);	
	}
	return 0;
}
