#include <stdio.h>
#include <queue>

using namespace std;
int n, dir[6][3], dist[200][200][200], beverage[200][200][200];
char cube[200][200][200];
typedef struct dot{
	int x, y, z, bev;
} Dot;
queue<Dot> qu;

void mktable(){
	for (int i = 0; i < 6; i++)
		for (int j = 0; j < 3; j++){
			if (i / 2 == j){
				dir[i][j] = 2 * (i % 2) - 1;
				continue;			
			}
			dir[i][j] = 0;
		}
	return;
}
void print(){
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++) {
			for (int k = 0; k < n; k++)
				printf("%c", cube[i][j][k]);
			puts("");
		}
	return;
}

void solve(){
	//find start exit
	Dot start, exit;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			for (int k = 0; k < n; k++){
				if (cube[i][j][k] == '.' || cube[i][j][k] == 'B')
					dist[i][j][k] = -1;
				else if (cube[i][j][k] == '#')
					dist[i][j][k] = 0;
				else if (cube[i][j][k] == 'S'){
					start.x = i;
					start.y = j;
					start.z = k;
					start.bev = 0;
					dist[i][j][k] = 0;
				}
				else if (cube[i][j][k] == 'E'){
					exit.x = i;
					exit.y = j;
					exit.z = k;
					dist[i][j][k] = -1;
				}
				beverage[i][j][k] = -1;
			}
	qu.push(start);
	//processing
	Dot now, next;
	int x, y, z, ansdist = -1, ansbev, flag = 0;
	while (1){
		if (qu.empty()){
			puts("Fail OAQ");
			return;
		}
		now = qu.front();
		if (dist[now.x][now.y][now.z] == ansdist){
			printf("%d %d\n", ansdist, ansbev);
			return;			
		}
		if (now.bev < beverage[now.x][now.y][now.z]){
			qu.pop();
			continue;		
		}
//		printf("processing (%d %d %d)...\n", now.x, now.y, now.z);
		for (int i = 0; i < 6; i++){
			x = now.x + dir[i][0];	
			y = now.y + dir[i][1];	
			z = now.z + dir[i][2];
//			printf("%d %d %d\n", x, y, z);
			if (x < 0 || y < 0 || z < 0
				|| x >= n || y >= n || z >= n	//out of range
				|| cube[x][y][z] == '#'	//trap
				|| !(dist[x][y][z] == -1 || dist[x][y][z] == dist[now.x][now.y][now.z] + 1)) //repeat
				continue;
//			printf("success\n");
			if (cube[x][y][z] == 'E'){	//exit
				if (flag == 0){
					ansdist = dist[now.x][now.y][now.z] + 1;
					ansbev = now.bev;
					flag = 1;
				}
				ansbev = (ansbev < now.bev) ? now.bev : ansbev;
			}
			if (dist[x][y][z] == -1)
				dist[x][y][z] = dist[now.x][now.y][now.z] + 1;
			next.x = x;
			next.y = y;
			next.z = z;
			if (cube[x][y][z] == 'B')
				next.bev = now.bev + 1;
			else
				next.bev = now.bev;
			if (next.bev <= beverage[x][y][z])
				continue;
			beverage[x][y][z] = next.bev;
			qu.push(next);
		}
		qu.pop();
//		puts("-------a point is done------");
	}
	return;
}
int main(void){
	int testcase;
	char b;
	mktable();
	scanf("%d", &testcase);
	while (testcase--){
		scanf("%d", &n);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				scanf("%s", cube[i][j]);
		solve();
		while (!qu.empty())
			qu.pop();
	}
	return 0;
}
