#include <stdio.h>
#include <list>
#include <vector>
using namespace std;
typedef struct edge{
	int a, b, weight;
} Edge;
list<Edge> edgeList;
int nodeNum, edgeNum, maxWeight, maxBit;
list<Edge>::iterator it;
int boss[100010], groupNum;

void findMaxBit(){
	maxBit = 0;
	while (maxWeight > 0){
		maxWeight >>= 1;
		maxBit++;
	}
	return;
}
int getBit(int num, int digit){
	return ((num >> (digit - 1)) & 1);
}
void init(){
	for (int i = 1; i <= nodeNum; i++)
		boss[i] = i;
	groupNum = nodeNum;
	return;
}
int findBoss(int a){
	if (a == boss[a])
		return a;
	boss[a] = findBoss(boss[a]);
	return boss[a];
}
void join(int a, int b){
	int aBoss = findBoss(a);
	int bBoss = findBoss(b);
	if (aBoss == bBoss)
		return;
	boss[aBoss] = bBoss;
	groupNum--;
	return;
}

void solve(){
	vector<list<Edge>::iterator> deleteV;
	vector<list<Edge>::iterator>::iterator dit;
	int deleteNum = 0;
	findMaxBit();
	Edge now;
	bool connect;
	int ans = 0;
	for (int i = maxBit; i >= 1; i--){
		init();
		for (it = edgeList.begin(); it != edgeList.end(); it++){
			now = (*it);
			if (getBit(now.weight, i) == 0)
				join(now.a, now.b);
			else {
				deleteV.push_back(it);
				deleteNum++;
			}
		}
		connect = (groupNum == 1) ? true : false;
		if (connect)	//delete edges //this bit is 0
			for (int j = 0; j < deleteNum; j++)
					edgeList.erase(deleteV[j]);
		else 	//this bit is 1
			ans |= (1 << (i - 1));
		deleteV.clear();
		deleteNum = 0;
	}
	printf("%d\n", ans);
	return;
}
int main(void){
	int testCase;
	Edge temp;
	scanf("%d", &testCase);
	while (testCase--){
		maxWeight = 0;
		scanf("%d%d", &nodeNum, &edgeNum);
		for (int i = 0; i < edgeNum; i++){
			scanf("%d%d%d", &temp.a, &temp.b, &temp.weight);
			if (temp.a == temp.b)
				continue;
			maxWeight = (maxWeight < temp.weight) ? temp.weight : maxWeight;
			edgeList.push_back(temp);
		}
		solve();
		edgeList.clear();
	}
	return 0;
}
