#include <stdio.h>
int main(void){
	long long int a, b;
	int testCase;
	scanf("%d", &testCase);
	for (int i = 0; i < testCase; i++){
		scanf("%lld%lld", &a, &b);
		printf("%lld\n", a + b);
	}
	return 0;
}
