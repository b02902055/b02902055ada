#include <stdio.h>
#include <algorithm>
#define MAXSTUNUM 100010
using namespace std;

typedef struct student{
	int intel;
	int boss;
}	Student;
Student a[MAXSTUNUM];
int stunum;

int findBoss(int i, int intel){
	if (a[a[i].boss].intel > intel)
		return a[i].boss;
	else
		return findBoss(a[i].boss, intel);
}
int main(void){
	int t, maxIntel, start;
	scanf("%d", &t);
	while (t--){
		scanf("%d", &stunum);
		maxIntel = 0;
		for (int i = 1; i <= stunum; i++){
			scanf("%d", &a[i].intel);
			a[i].boss = i - 1;
			maxIntel = (a[i].intel > maxIntel) ? a[i].intel : maxIntel;
		}
		a[1].boss = stunum;
		start = -1;
		for (int i = 1; i <= stunum; i++)
			if (a[i].intel == maxIntel){
				a[i].boss = 0;
				if (start == -1)
					start = i;
			}
		for (int i = (start == stunum) ? 1 : start + 1;;i++){
			if (i == start)
				break;
			if (a[i].boss == 0){
				if (i == stunum)
					i = 0;
				continue;
			}
			a[i].boss = findBoss(i, a[i].intel);
			if (i == stunum)
				i = 0;
		}
		printf("%d", a[1].boss);
		for (int i = 2; i <= stunum; i++)
			printf(" %d", a[i].boss);
		puts("");
	}
	return 0;
}
