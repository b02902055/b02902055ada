#include <stdio.h>
#include <set>
#include <vector>
#include <algorithm>
#define blockMax 100010
#define playerMax 100010
#define eventMax 100010
#define ON 1
#define OFF 0
using namespace std;
int ans[playerMax];
long long int goal[playerMax];
set<int>::iterator it;

int count(set<int>::iterator a, set<int>& s){
	set<int>::iterator bit;
	int count = 0;
	for (bit = s.begin(); bit != a; bit++)
		count++;
	return count;
}
void init_t(int t[][2], int num){
	for (int i = 0; i <= num; i++){
		t[i][0] = 0;
		t[i][1] = 0;
	}
	return;
}
void init(int* a, int num, int status){
	for (int i = 1; i <= num; i++)
		a[i] = status;
	return;
}
void print(int* a, int num){
	if (num == 0)
		return;
	printf("%d", a[1]);
	for (int i = 2; i <= num; i++)
		printf(" %d", a[i]);
	puts("");
	return;		
}
void init_l(long long int* a, int num, int status){
	for (int i = 1; i <= num; i++)
		a[i] = status;
	return;
}
void print_l(long long int* a, int num){
	if (num == 0)
		return;
	printf("%lld", a[1]);
	for (int i = 2; i <= num; i++)
		printf(" %lld", a[i]);
	puts("");
	return;		
}
void print_v(vector<int>& v){
	for (int i = 0; i < v.size(); i++)
		printf("%d ", v[i]);
	puts("");
	return;
}
void print_e(int e[][4], int num){
	for (int i = 1; i <= num; i++){
		for (int j = 0; j < 4; j++)
			printf("%d ", e[i][j]);
		puts("");
	}
	return;
}
int binary(int a, vector<int>& v){
	int l = 0, r = v.size() - 1, mid;
	if (v.size() == 1)
		return r;
	while (r - l > 1){
		mid = (l + r) / 2;
		if (v[mid] == a)
			return mid;
		else if (v[mid] < a)
			l = mid;
		else if (v[mid] > a)
			r = mid - 1;
	}
	if (v.size() == 2 && v[l] == a)
		return l;
	if (r == v.size() - 1)
		return r;
	if (v[r] == a)
		return r;
	return l;
}
void print_s(set<int>& s){
	set<int>::iterator it;
	for (it = s.begin(); it != s.end(); it++)
		printf("%d ", *it);
	puts("");
	return;
}
void solve(int* block, int event[][4], int playerNum, int blockNum, int eventNum, int ptag[][2], set<int>& player){

//	printf("xxxxxxxxxxxxxxx\n");
//	printf("playerNum = %d\n", playerNum);
//	for (int i = 1; i <= eventNum; i++)
//		printf("%d %d %d\n", event[i][0], event[i][1], event[i][2], event[i][3]);
//	printf("--------------\n");
	if (playerNum == 0)
		return;

	int end = eventNum;
	int cut = (1 + end) / 2;
	int blocks[blockNum + 1], events[eventNum / 2 + 1][4], playerNums = 0;
	int blockf[blockNum + 1], eventf[eventNum / 2 + 1][4], playerNumf = 0;
//  collect the tags
	int tag[blockNum + 1][2];
	init_t(tag, blockNum);
	for (int i = 1; i <= cut; i++){
		tag[event[i][0]][0] += event[i][2];
		tag[event[i][1]][1] -= event[i][2];
	}
	for (int i = 1; i <= blockNum; i++){
		tag[i][0] += ptag[i][0];
		tag[i][1] += ptag[i][1];
	}
//	puts("tag");
//	for (int i = 1; i <= blockNum; i++)
//		printf("%d %d\n", tag[i][0], tag[i][1]);
//
//	printf("cut = %d\n", cut);
//	count player own
	
	long long int playerown[playerNum];
	long long int add = 0;
	for (int i = 0; i < playerNum; i++)
		playerown[i] = 0;
	for (int i = 1; i <= blockNum; i++){
		add += tag[i][0];
		it = player.find(block[i]);
		playerown[count(it, player)] += add;
		add += tag[i][1];
	}

//	printf("player: ");
//	print_s(player);
//	for (int i = 0; i < playerNum; i++)
//		printf("%d ", playerown[i]);
//	puts("");
//	eventNum == 1
	if (eventNum == 2){
		int i = 0;
		for (it = player.begin(); it != player.end(); it++, i++)
			if (playerown[i] >= goal[*it])
				ans[*it] = event[1][3];
			else
				ans[*it] = event[1][3] + 1;
		return;
	}
//	split the player
	set<int> success, fail;
	for (it = player.begin(); it != player.end(); it++){
		if (playerown[count(it, player)] >= goal[*it]){ // success
			success.insert(*it);
			playerNums++;
		}
		else {
			fail.insert(*it);
			playerNumf++;
		}
	}

//	split the Plot

	vector<int> successPlot, failPlot;
	for (int i = 1; i <= blockNum; i++)
		if (success.find(block[i]) != success.end())
			successPlot.push_back(i);
		else
			failPlot.push_back(i);

//	sort(successPlot.begin(), successPlot.end(), compare);
//	sort(failPlot.begin(), failPlot.end(), compare);

//make table: block, event
	for (int i = 0; i < successPlot.size(); i++)
		blocks[i + 1] = block[successPlot[i]];
	for (int i = 1; i <= cut; i++){
		events[i][0] = binary(event[i][0], successPlot) + 1;
		events[i][1] = binary(event[i][1], successPlot) + 1;
		events[i][2] = event[i][2];
		events[i][3] = event[i][3];
	}
	for (int i = 0; i < failPlot.size(); i++)
		blockf[i + 1] = block[failPlot[i]];
	for (int i = cut + 1; i <= end; i++){
		eventf[i - cut][0] = binary(event[i][0], failPlot) + 1;
		eventf[i - cut][1] = binary(event[i][1], failPlot) + 1;
		eventf[i - cut][2] = event[i][2];
		eventf[i - cut][3] = event[i][3];
	}
//	printf("event: \n");	
//	print_e(events, cut);
//	printf("~~~~~\n");
//	print_e(eventf, end - cut);
//	make tag
	int t = 0, up = 0, down = 0;
	int nptag[successPlot.size() + 5][2], ntag[failPlot.size() + 5][2];
	for (int i = 1; i <= blockNum && t < successPlot.size(); i++){
		up += ptag[i][0];
		down += ptag[i][1];
		if (i == successPlot[t]){
			nptag[t][0] = up;
			nptag[t][1] = down;
			up = 0;
			down = 0;
			t++;
		}
	}
	t = 0;
	up = 0;
	down = 0;
	for (int i = 1; i <= blockNum && t < failPlot.size(); i++){
		up += tag[i][0];
		down += tag[i][1];
		if (i == failPlot[t]){
			ntag[t][0] = up;
			ntag[t][1] = down;
			up = 0;
			down = 0;
			t++;
		}
	}



// 	solve
	solve(blocks, events, playerNums, successPlot.size(), cut, ptag, success);
	solve(blockf, eventf, playerNumf, failPlot.size(), end - cut, tag, fail);
	return;
}
int main(void){
	int testCase;
	int playerNum, blockNum, eventNum;
	int block[blockMax], event[eventMax][4], tag[blockMax][2];
	scanf("%d", &testCase);
	while (testCase--){
		scanf("%d%d%d", &playerNum, &blockNum, &eventNum);
		init(ans, playerNum, -1);
		for(int i = 1; i <= playerNum; i++)	//goal
			scanf("%lld", &goal[i]);
		for (int i = 1; i <= blockNum; i++)//owner
			scanf("%d", &block[i]);
		for (int i = 1; i <= eventNum; i++){//event
			event[i][3] = i;
			scanf("%d%d%d", &event[i][0], &event[i][1], &event[i][2]);
		}
		init_t(tag, blockNum);


		int atag[blockNum + 1][2];
		init_t(atag, blockNum);
		for (int i = 1; i <= eventNum; i++){
			atag[event[i][0]][0] += event[i][2];
			atag[event[i][1]][1] -= event[i][2];
		}
		long long int player[playerNum + 5];
		long long int add = 0;
		init_l(player, playerNum, 0);
		for (int i = 1; i <= blockNum; i++){
			add += atag[i][0];
			player[block[i]] += add;
			add += atag[i][1];
		}
		if (eventNum == 1){
			for (int id = 1; id <= playerNum; id++)
				if (player[id] >= goal[id])
					ans[id] = 1;
				else
					ans[id] = -1;
			print(ans, playerNum);
			continue;
		}
//		print_l(player, playerNum);

		int playerNums = 0;
		set<int> success;
		for (int id = 1; id <= playerNum; id++)
			if (player[id] >= goal[id]){ // success
				success.insert(id);
				playerNums++;
			}
		vector<int> successPlot;
		for (int i = 1; i <= blockNum; i++)
			if (success.find(block[i]) != success.end())
				successPlot.push_back(i);
//		sort(successPlot.begin(), successPlot.end(), compare);
		int blocks[blockNum + 5], events[eventNum + 5][4];
		for (int i = 0; i < successPlot.size(); i++)
			blocks[i + 1] = block[successPlot[i]];
		for (int i = 1; i <= eventNum; i++){
			events[i][0] = binary(event[i][0], successPlot) + 1;
			events[i][1] = binary(event[i][1], successPlot) + 1;
			events[i][2] = event[i][2];
			events[i][3] = event[i][3];
		}
//		print(blocks, successPlot.size());
//		print_e(events, eventNum);
//		print_v(successPlot);

		solve(blocks, events, playerNums, successPlot.size(), eventNum, tag, success);
//		printf("#############################\n");
		print(ans, playerNum);
	}
	return 0;
}
