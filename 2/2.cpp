#include <stdio.h>
#include <vector>
#include <utility>
#include <algorithm>
#define MAX 1000000
#define NEW 1
#define OLD 0

using namespace std;
vector<pair<int, int> > box(MAX);

bool compare(pair<int, int> a, pair<int, int> b){
	if (a.first < b.first || (a.first == b.first && a.second < b.second))
		return true;
	return false;
}
void swap(int& a, int& b){
	int temp = a;
	a = b;
	b = temp;
	return;
}
void pswap(pair<int, int>& a, pair<int, int>& b){
	pair<int, int> temp = a;
	a = b;
	b = temp;
	return;
}
long long int combine(int start, int mid, int end){
	
//	puts("");
//	printf("%d %d %d\n", start, mid, end);
//	puts("cut");
//	for (int i = start; i < end + 1; i++)
//		printf("%d %d\n", box[i].second, box[i].first);
//	puts("");

	long long int ans = 0;
	if (end - start <= 1){
		if (box[start].second > box[end].second)
			pswap(box[start], box[end]);
		else if (box[start].second < box[end].second)
			ans++;
		else if (box[start].second == box[end].second)
			if (box[start].first == box[end].first)
				ans += 2;
			else {
				if (box[start].first > box[end].first)
					pswap(box[start], box[end]);
				ans++;
			}
//		printf("ans = %lld\n", ans);
		return ans;	
	}
	vector<pair<int, int> > temp(end - start + 5);
	int i = 0;
	int p1 = start, p2 = mid + 1;
	int flag = NEW;
	long long int last = 0;
	while (p1 != mid + 1 && p2 != end + 1){
		if (box[p1].second < box[p2].second){
			temp[i] = box[p1];
			if (flag == NEW){
				ans += p1 - start + 1;
				last += p1 - start + 1;
				flag = OLD;
			}
			else {
				ans += 1;
				last += 1;
			}
			p1++;
		}
		else if (box[p1].second > box[p2].second){
			if (flag == NEW){
				temp[i] = box[p2];
				p2++;
				ans += p1 - start;
			}
			else {
				temp[i] = box[p2];
				i++;
				p2++;
				for (; p2 != end + 1 && box[p2].second == box[p2 - 1].second
						&& box[p2].first == box[p2 - 1].first; p2++, i++){
					ans += last;
					temp[i] = box[p2];
				}
				i--;
			}
			last = 0;
			flag = NEW;
		}
		else { // second the same, check first
			if (flag == NEW){
				ans += p1 - start;
				last += p1 - start;
				flag = OLD;
			}
			if (box[p1].first == box[p2].first){
				ans += 2;
				last += 2;
				temp[i] = box[p1];
				p1++;
			}
			else {
				ans += 1;
				last += 1;
				if (box[p1].first <= box[p2].first){
					temp[i] = box[p1];		
					p1++;
				}
				else {
					temp[i] = box[p2];
					p2++;
				}
			}
		}
		i++;
//		printf("now, ans = %lld\n", ans);
	}
//	printf("here, ans = %lld\n", ans);
	if (p1 == mid + 1){
		temp[i] = box[p2];
		p2++;
		i++;
		for (; p2 != end + 1 && box[p2].second == box[p2 - 1].second
				&& box[p2].first == box[p2 - 1].first; p2++, i++){
			ans += last;
			temp[i] = box[p2];
		}
			
		for (; p2 != end + 1; p2++, i++){
			ans += mid - start + 1;
			temp[i] = box[p2];
		}
//		int flag = i;
//		for (; p2 != end + 1; p2++, i++){
//			temp[i] = box[p2];
//			if (i != flag){
//				for (int j = mid; box[j].second == box[p2].second; j--)
//					if (box[j].first == box[p2].first)
//						ans++;
//					else
//						break;
//				ans += (mid - start + 1);
//			}
//		}
	}
	else if (p2 == end + 1)
		for (; p1 != mid + 1; p1++, i++)
			temp[i] = box[p1];
	for (int j = 0; j < i; j++)
		box[start + j] = temp[j];
//	printf("ans = %lld\n", ans);
	return ans;
}
long long int solve(int start, int end){
	if (start < end){
		int mid = (start + end) / 2;
		return solve(start, mid) + solve(mid + 1, end) + combine(start, mid, end);
	}
	return 0; 
}
int main(void){
	pair<int, int> temp;
	int testCase, w, h, boxNum;
	scanf("%d", &testCase);
	while(testCase--){
		scanf("%d", &boxNum);
		for (int i = 0; i < boxNum; i++){
			scanf("%d%d", &w, &h);
			if (w > h)
				swap(w, h);
			temp = make_pair(w, h);
			box[i] = temp;
		}
		sort(&box[0], &box[boxNum], compare);

//		printf("sort\n");
//		for (int i = 0; i < boxNum; i++)
//			printf("%d %d\n", box[i].first, box[i].second);
//		printf("sort\n");

		printf("%lld\n", solve(0, boxNum - 1));
	}
	return 0;
}
