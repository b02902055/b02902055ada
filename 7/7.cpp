#include <stdio.h>

int coin[10], table[10];

void mktable(){
	table[0] = 1;
	table[1] = 5;
	table[2] = 10;
	table[3] = 20;
	table[4] = 50;
	table[5] = 100;
	table[6] = 200;
	table[7] = 500;
	table[8] = 1000;
	table[9] = 2000;
	return;
}
int countSum(){
	int sum = 0;
	for (int i = 0; i < 10; i++)
		sum += coin[i] * table[i];
	return sum;
}
int solve(int toPay, int i){
	if (i < 0){
		if (toPay > 0)
			return -1;
		else 
			return 0;	
	}
	int target = toPay / table[i];
	int case1 = (coin[i] >= target) ? target : coin[i];
	if (case1 == 0)
		return solve(toPay, i - 1);
	int solve1 = solve(toPay - case1 * table[i], i - 1);
	int case11 = (solve1 == -1) ? -1 : case1 + solve1;
	if (i < 3)
		return case11;
	int solve2 = solve(toPay - (case1 - 1) * table[i], i - 1);
	int case22 = (solve2 == -1) ? -1 : case1 - 1 + solve2;
	if (case11 == -1 && case22 == -1)
		return -1;
	if (case11 == -1)
		return case22;
	if (case22 == -1)
		return case11;
	return (case11 < case22) ? case11 : case22;
}
int main(void){
	mktable();
	int testCase, toPay, totalMoney, totalCoin, ans;
	scanf("%d", &testCase);
	while (testCase--){
		totalCoin = 0;
		scanf("%d", &toPay);
		for (int i = 0; i < 10; i++){
			scanf("%d", &coin[i]);
			totalCoin += coin[i];
		}
		totalMoney = countSum();
		if (toPay > totalMoney){
			puts("-1");
			continue;
		}
		ans = solve(totalMoney - toPay, 9);
		if (ans == -1){
			puts("-1");
			continue;
		}
		printf("%d\n", totalCoin - ans);	
	}
	return 0;
}
