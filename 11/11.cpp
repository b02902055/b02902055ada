#include <stdio.h>
#include <vector>
#include <algorithm>
#define MAXENUM 10000
#define MAXVNUM 110
using namespace std;
vector<int> v[MAXVNUM], u, all;


int vNum, eNum, nvNum, m;

vector<int> in(int i, vector<int> s){
	int now = s[i];
	vector<int> ans;
	vector<int>::iterator vit = v[now].begin(), sit = s.begin();
	while (i--)
		sit++;
	while (vit != v[now].end() && sit != s.end()){
		if (*vit == *sit){
			ans.push_back(*vit);
			vit++;
			sit++;		
		}
		else if (*vit < *sit)
			vit++;
		else
			sit++;
	}
	return ans;
}
void solve(vector<int> s){
	int ss = s.size();
	int us = u.size();
	if (ss == 0){
//		printf("find a cg: ");
//		vector<int>::iterator it;
//		for (it = u.begin(); it != u.end(); it++)
//			printf("%d ", *it);
//		puts("");
		m = (us > m) ? us : m;
		return;
	}
	for (int i = 0; i < s.size(); i++){
		if (us + ss - i <= m)
			return;
		u.push_back(s[i]);
		solve(in(i, s));	
		u.pop_back();
	}
	return;
}
void eatEdge(){
	int a, b;
	for (int i = 0; i < eNum; i++)
		scanf("%d%d", &a, &b);
	return;
}
void makev(){
	int t[vNum][vNum], a, b;
	for (int i = 0; i < vNum; i++)
		for (int j = 0; j < vNum; j++)
			t[i][j] = (i == j) ? 1 : 0;
	for (int i = 0; i < eNum; i++){
		scanf("%d%d", &a, &b);
		t[a][b] = 1;
		t[b][a] = 1;
	}
	for (int i = 0; i < vNum; i++)
		for (int j = 0; j < vNum; j++)
			if (t[i][j] == 1)
				t[i][j] = 0;
			else
				t[i][j] = 1;
	for (int i = 0; i < vNum; i++)
		v[i].clear();
	all.clear();
	int flag;
	for (int i = 0; i < vNum; i++){
		flag = 0;
		for (int j = 0; j < vNum; j++)
			if (t[i][j] == 1){
				v[i].push_back(j);
				flag = 1;
			}
		if (flag == 1)
			all.push_back(i);
	}
	/*

	for (int i = 0; i < vNum; i++){
		for (int j = 0; j < vNum; j++)
			printf("%d ", t[i][j]);
		puts("");
	}
	vector<int>::iterator it;

	for (int i = 0; i < vNum; i++){
		for (it = v[i].begin(); it != v[i].end(); it++)
			printf("%d ", *it);
		puts("");
	}
	for (it = all.begin(); it != all.end(); it++)
		printf("%d ", *it);
	puts("");*/

	
	return;
}
int main(void){
	int t, a, b;
	scanf("%d", &t);
	while (t--){
		scanf("%d%d", &vNum, &eNum);
		if (vNum == 1){
			puts("1");
			eatEdge();
			continue;		
		}
		if (eNum == 0){
			printf("%d\n", vNum);
			continue;		
		}
		if (eNum == 1){
			printf("%d\n", vNum - 1);
			eatEdge();
			continue;		
		}
		makev();
		m = 0;
		for (int i = 0; i < all.size(); i++){
			u.push_back(all[i]);
			solve(in(i, all));
			u.pop_back();
		}
		printf("%d\n", m);
	}
	return 0;
}
